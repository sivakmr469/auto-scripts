#!/bin/bash
DATE=$(date +%F)
USER_ID=$(id -u)
R="\e[31m"
G="\e[32m"
N="\e[0m"
Y="\e[33m"
LOG=stack.log

if [ $USER_ID -ne 0 ]; then
	echo -e "...$R You should run this script as root user...$N"
else
	echo -e "...$G logged in as root user...$N"
fi

VALIDATE(){
	if [ $1 -ne 0 ]; then
		echo -e "$2 ...$R FAILURE $N"
		exit 1
	else
		echo -e "$2 ... $G SUCCESS $N"
	fi
}

yum update -y &>> $LOG

VALIDATE $? "Updating yum"

amazon-linux-extras install java-openjdk11 -y &>> $LOG

VALIDATE $? "Installing JDK-11"
#with out this jenkins will not be installed
amazon-linux-extras install epel -y &>> $LOG
VALIDATE $? "Installing EPEL"
yum install git -y &>> $LOG
VALIDATE $? "Installing GIT"
wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo &>> $LOG
VALIDATE $? "Adding jenkins repo"
rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key &>> $LOG
VALIDATE $? "importing jenkins key"
yum upgrade -y &>> $LOG
VALIDATE $? "Upgrade yum"
yum install jenkins -y &>> $LOG
VALIDATE $? "Installing Jenkins"
systemctl enable jenkins &>> $LOG
VALIDATE $? "Enable Jenkins"
systemctl start jenkins &>> $LOG
VALIDATE $? "Starting Jenkins"
#set up jenkins
#attach registry role
#vim ~/.bashrc
#JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.12.0.7-0.amzn2.0.2.x86_64
#PATH=$PATH:$JAVA_HOME/bin
#source ~/.bashrc
#
#go to security configuration and enable proxy, JNLP
#Install pipeline utilities, SonarQube plugins
#Define global libraries, sonar server creds
#create bitbucket credentilas
#create sonarqube token