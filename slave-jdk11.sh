#!/bin/bash
DATE=$(date +%F)
USER_ID=$(id -u)
R="\e[31m"
G="\e[32m"
N="\e[0m"
Y="\e[33m"
LOG=stack.log
MAVEN_URL=https://dlcdn.apache.org/maven/maven-3/3.8.3/binaries/apache-maven-3.8.3-bin.tar.gz
MAVEN_FILE=$(echo $MAVEN_URL | awk -F "/" '{print $NF}') #echo $TOMCAT_URL | awk -F "/" '{print $NF}'
echo "Maven file is $MAVEN_FILE"
MAVEN_FOLDER=$(echo $MAVEN_FILE | sed -e 's/.tar.gz//')

if [ $USER_ID -ne 0 ]; then
	echo -e "...$R You should run this script as root user...$N"
else
	echo -e "...$G logged in as root user...$N"
fi

VALIDATE(){
	if [ $1 -ne 0 ]; then
		echo -e "$2 ...$R FAILURE $N"
		exit 1
	else
		echo -e "$2 ... $G SUCCESS $N"
	fi
}

yum update -y &>> $LOG

VALIDATE $? "Updating yum"

amazon-linux-extras install java-openjdk11 -y &>> $LOG

VALIDATE $? "Installing JDK-11"
#with out this jenkins will not be installed
amazon-linux-extras install epel -y &>> $LOG
VALIDATE $? "Installing EPEL"
yum install git -y &>> $LOG
cd /opt
wget $MAVEN_URL &>> $LOG
VALIDATE $? "Downloading Maven"
tar xzvf $MAVEN_FILE &>> $LOG
M2_HOME=/opt//usr/lib/jvm/java-11-openjdk-11.0.12.0.7-0.amzn2.0.2.x86_64$MAVEN_FOLDER
echo $M2_HOME
amazon-linux-extras install docker -y &>> $LOG
VALIDATE $? "Installing Docker"
systemctl enable docker &>> $LOG
VALIDATE $? "Enable docker"
systemctl start docker &>> $LOG
VALIDATE $? "Start Docker"
usermod -a -G docker ec2-user &>> $LOG
VALIDATE $? "Adding ec2-user to docker group"
docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:latest
VALIDATE $? "Running SonarQube"